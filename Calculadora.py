__author__ = 'jfmurcia'


class Calculadora:
    def sumar(self,cadena):
        if cadena =='':
            return 0
        elif not(cadena.isnumeric()) and not "&" in cadena and not ":" in cadena:
            cadena2=cadena.split(",")
            cadena2=map(int,cadena2)
            resultado = sum(cadena2)
            return int(resultado)
        elif not(cadena.isnumeric()) and ("&" in cadena or not ":" in cadena):
            separador = ","
            cadena2 = cadena.split(separador)
            if len(cadena2) > 1:
                valor1 = int(cadena2[0])
                valor2 = int(cadena2[1])
                resultado = int(valor1 + valor2)
            separador =":"
            cadena2 = cadena.split(separador)
            if len(cadena2) > 1:
                valor1 = int(cadena2[0])
                valor2 = int(cadena2[1])
                resultado = int(valor1 + valor2)
            separador = "&"
            cadena2 = cadena.split(separador)
            if len(cadena2) > 1:
                valor1 = int(cadena2[0])
                valor2 = int(cadena2[1])
                resultado = int(valor1 + valor2)
            return resultado
        else:
            return int(cadena)
