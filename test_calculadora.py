from unittest import TestCase
from Calculadora import Calculadora

__author__ = 'jfmurcia'


class TestCalculadora(TestCase):
    def test_sumar_vacia(self):
        self.assertEqual(Calculadora().sumar(""), 0, "Cadena vacia")

    def test_sumar_cadenaConUnNumero(self):
        self.assertEqual(Calculadora().sumar("1"), 1, "Un numero")
        self.assertEqual(Calculadora().sumar("2"), 2, "Un numero")

    def test_sumar_cadenaConDosNumero(self):
        cadena = "1,2"
        cadena2 = cadena.split(",")
        valor1 = int(cadena2[0])
        valor2 = int(cadena2[1])
        resultado = int(valor1 + valor2)
        self.assertEqual(Calculadora().sumar(cadena), resultado, "Suma dos numeros")

    def test_sumar_cadenaConVariosNumero(self):
        cadena = "1,2,5,7"
        cadena2 = cadena.split(",")
        cadena2 = map(int, cadena2)
        resultado = sum(cadena2)
        self.assertEqual(Calculadora().sumar(cadena), resultado, "Suma varios Numeros")

    def test_sumar_cadenaOtroSeparador(self):
        cadena = "1&2"
        resultado = 0
        separador = ","
        cadena2 = cadena.split(separador)
        if len(cadena2) > 1:
            valor1 = int(cadena2[0])
            valor2 = int(cadena2[1])
            resultado = int(valor1 + valor2)
        separador =":"
        cadena2 = cadena.split(separador)
        if len(cadena2) > 1:
            valor1 = int(cadena2[0])
            valor2 = int(cadena2[1])
            resultado = int(valor1 + valor2)
        separador = "&"
        cadena2 = cadena.split(separador)
        if len(cadena2) > 1:
            valor1 = int(cadena2[0])
            valor2 = int(cadena2[1])
            resultado = int(valor1 + valor2)
        self.assertEqual(Calculadora().sumar(cadena), resultado, "Suma varios Numeros")

if __name__ == '__main__':
    unittest.main()
